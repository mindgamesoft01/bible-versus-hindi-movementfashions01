package com.movement.bibleversus.hindiversus

class CustomException(message: String) : Exception(message)
